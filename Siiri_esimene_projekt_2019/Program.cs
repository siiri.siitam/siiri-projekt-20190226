﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siiri_esimene_projekt_2019
{
    class Program
    {
        static void Main(string[] args)
        {
            //siit ülespoole las olla nii nagu on - hiljem vaatame

            // muutujad ja tehted
            int a = 1;
            int b = 2;
            int c = a + b;

            Console.WriteLine(c);

            byte d = 18 % 3;
            byte e = 19 % 3;
            byte f = 20 % 3;
            byte g = 21 % 3;

            Console.WriteLine(d);
            Console.WriteLine(e);
            Console.WriteLine(f);
            Console.WriteLine(g);

            Console.WriteLine(a += b += 3);

            // see on elvistehe

            decimal palk = 1000;
            decimal tulumaks = (palk > 500) ? (palk - 500) * 0.2M : 0;
            Console.WriteLine(tulumaks);
            // natuke tekstidest ehk stringidest

            // ++ järgmine (+1) operandi suurendatakse ühe võrra 
            // char on kahebaidiline unicode märk
            char ahaa = 'A';
            Console.WriteLine(++ahaa);
            // string andmetüübina
            // stringi literaal kirjutatakse alati jutumärkide vahele ""

            string nimi = "Siiri Siitam";
            Console.WriteLine(nimi);

            //string tekst = "Kui Arno ütles \"Kentuki lõvi\" siis see jäigi Tootsile külge";
            //Console.WriteLine(tekst);
            //tekst1 = "jutumärgid (\") tuleb katta kaldkriipsuga \\";
            //Console.WriteLine(tekst1);
            //string failinimi = @"C:\Users\sarvi\Onedrive";

            string üksNimi = "Siiri";
            Console.WriteLine("Ütle üks nimi: ");
            string teineNimi = Console.ReadLine();
            
            Console.WriteLine(üksNimi.ToLower() == teineNimi.ToLower() ? "on samad" : "ei ole samad");
            // Console.WriteLine(üksNimi == teineNimi ? "on samad" : "on erinevad");
            //Console.WriteLine(üksNimi.Equals(teineNimi, StringComparison.CurrentCultureIngnoreCase);


            //siit allapoole las olla nii nagu on - hiljem vaatame
        }

    }
}
